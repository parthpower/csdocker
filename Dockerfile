FROM amd64/ubuntu:16.04

ENV HLDS_PARA=""

EXPOSE 27015/tcp
EXPOSE 27015/udp

EXPOSE 26900/udp
EXPOSE 26900/tcp

RUN apt-get update && \
    apt-get -y install curl lib32gcc1

WORKDIR /Steam

RUN curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf -; /bin/bash -c "./steamcmd.sh +login anonymous +force_install_dir ./hlds +app_set_config 90 mod cstrike +app_update 90 validate +exit";

RUN /bin/bash -c "./steamcmd.sh +login anonymous +force_install_dir ./hlds +app_set_config 90 mod cstrike +app_update 90 validate +exit"

WORKDIR /Steam/hlds
ENTRYPOINT ["./hlds_run"]
CMD ["-game cstrike -console +map de_dust +ip 0.0.0.0 +rcon_password 1234"]
